<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Ejercicio 4</title>
</head>
<style>

    .max {
        color: green;
    }
    .min {
        color: red;
    }

</style>

<body>
    <?php
        mt_srand(23536); 

        $var1 = mt_rand(50,900);
        $var2 = mt_rand(50,900);
        $var3 = mt_rand(50,900);

        $varsArray = [$var1, $var2, $var3];
        
        rsort($varsArray);

        $lenght = count($varsArray);
        
        echo "<div>";
        foreach ($varsArray as $key => $value) {

            switch ($key) {
                case 0:
                    echo "<span class='max'>$value </span>";
                    break;
                case $lenght - 1:
                    echo "<span class='min'>$value </span>";
                    break;
                default:
                    echo "<span>$value </span>";
                    break;
            }

        }
        echo "</div>";
        
    ?>

  
</body>
</html>
