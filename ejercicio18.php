<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
<?php
    function leerArchivo($archivo){
        $gestor = fopen($archivo, "r");
        $contenido = fread($gestor, filesize($archivo));
        $visitas = (int) $contenido;
        $visitas++;
        fclose($gestor);
        return $visitas;
    }

    function actualizarVisitas($archivo,$visitas){
      
        $gestor = fopen($archivo, "w");
        fwrite($gestor, $visitas);
        fclose($gestor);
    }

    $cantidad = leerArchivo("visitas.txt");
    actualizarVisitas("visitas.txt",$cantidad);
    echo "Visitas: ".$cantidad;
    

   
?>
    
</body>
</html>