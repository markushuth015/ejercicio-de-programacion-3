<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Ejercicio 3</title>
</head>
<style>
table, th, td {
  border:1px solid black;
}
</style>
<body>
    <table>
        <?php
            $n=20;
        ?>
        <tr>
            <th>Números pares que existen entre 1 y <?php echo $n ?></th>
        </tr>
        <tr>

        
        <?php
            for ($i=1; $i < $n ; $i++) { 
                if($i%2==0 ){
                    echo "<tr>";
                    echo "<td> $i </td>";
                    echo "</tr>";
                }
            }
        ?>
        </tr>
    </table>
</body>
</html>
