<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercicio 1</title>
</head>
<body>
    <?php
    //A es la raiz cuadrada de 2
    $A = sqrt(2);
    //• ¶ es el número PI
    $¶ = M_PI;
    //• B es es la raíz cúbica de 3
    $B = pow(3,1/3);
    //• C es la constante de Euler
    $C = M_EULER;
    //•/ D es la constante e
    $D = M_E;
    $x = (($A * $¶) + $B) / ($C*$D);
    print $x
    ?>
    
</body>
</html>